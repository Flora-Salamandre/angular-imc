
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  poids = 0;
  taille = 0;
  imc = 0;
  tranche = 'une crêpe';
  differenceInf = 0;
  differenceSup = 0;
  color = "";

  calculerImc(){
    this.imc = this.poids / (this.taille * this.taille);
    if (this.imc < 18.5)
    {
      this.tranche = 'une crêpe';
      this.differenceSup = 18.5 * (this.taille * this.taille) - this.poids;
      this.color = 'success';
    }
    else if(this.imc < 25)
    {
      this.tranche = 'un Pitch';
      this.differenceSup = 25 * (this.taille * this.taille) - this.poids;
      this.differenceInf = this.poids - 18.5 * (this.taille * this.taille);
      this.color = 'warning';
    }
    else
    {
      this.tranche = 'une brioche';
      this.differenceInf = this.poids - 25 * (this.taille * this.taille);
      this.color = 'info';
    }
  }
}
